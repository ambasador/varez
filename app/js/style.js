$(document).ready(function(){
    $('.slider-baner').slick({
         dots: true,
         //autoplay: true,
        autoplaySpeed: 7000
    });
    $('.reviews').slick({
        dots: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 961,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 769,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
        ]
    }); 
     $('.slider-for').slick({
       slidesToShow: 1,
       slidesToScroll: 1,
       arrows: false,
       fade: true,
       asNavFor: '.slider-nav'
     });
     $('.slider-nav').slick({
       slidesToShow: 5,
       slidesToScroll: 1,
       asNavFor: '.slider-for',
       dots: false,
       focusOnSelect: true,
         responsive: [
            {
              breakpoint: 1801,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
              }
            }
        ]

     });
    
    
    
    /*$('.filter-content').on('click', function(){
            $(this).find('input[type=checkbox]').each(function(){
                
            var inputCheck = $(this).prop("checked");
            if(inputCheck == true){
            $(this).parents('.filter-one').addClass('active');
            }
                else{
                    $(this).parents('.filter-one').removeClass('active');
                }
            //var a = $(this).find('input[type=checkbox]').attr('checked');
        }); 
    });*/
    
    
    $('.catalog-tabs .close').on('click',function(){
        $(this).find('.btn-menu').toggleClass('active');
        $('.catalog-open').slideToggle();
    });
    
    $('.mobile-header .btn-menu').on('click',function(){
        if($('.mobile-header-open').hasClass('phones-active')){
            $('.intro-mobile-header-open').slideToggle(300);
            $('body').toggleClass('hidden');
            $('.mobile-header-open').removeClass('phones-active');
            $(this).toggleClass('active');
            $('body').toggleClass('hidden');
            $('.intro-mobile-header-open').slideToggle(300);
        }
        else{
            $(this).toggleClass('active');
            $('body').toggleClass('hidden');
            $('.intro-mobile-header-open').slideToggle(300);
        }
    });
    
    $('.mobile-header .phones').on('click',function(e){
        e.preventDefault();
        if($('.mobile-header .btn-menu').hasClass('active')){
            $('.mobile-header .btn-menu').toggleClass('active');
            $('body').toggleClass('hidden');
            $('.intro-mobile-header-open').slideToggle(300);
            $('.mobile-header-open').toggleClass('phones-active');
            $('body').toggleClass('hidden');
            $('.intro-mobile-header-open').slideToggle(300);
        }
        else{
            $('.mobile-header-open').toggleClass('phones-active');
            $('body').toggleClass('hidden');
            $('.intro-mobile-header-open').slideToggle(300);
        }

    });
    $('.filter-one h3').on('click', function(){
        $(this).find('i').toggleClass('active');
        $(this).parent().find('.filter-content').slideToggle();   
    });
    $('.mobile-section .filters').on('click', function(){
        $('.filters-mobile').slideToggle();
        $('.intro').toggleClass('active');
        $('body').toggleClass('hidden');
    });
    $('.filters-mobile .close').on('click', function(){
        $('.filters-mobile').slideToggle();
        $('.intro').toggleClass('active');
        $('body').toggleClass('hidden');
    });
    
    $('ul.tabs-capion').on('click', 'li:not(.active)', function() {
        $(this)
        .addClass('active').siblings().removeClass('active')
        .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
    
    $('.one-click-btn').on('click', function(e){
        e.preventDefault();
        $('.intro-all.intro-one-click').toggleClass('active');
        $('body').toggleClass('hidden');
    });
    $('.pop-up-one-click .btn-menu-pop-up').on('click', function(){
        $('.intro-all.intro-one-click').toggleClass('active');
        $('body').toggleClass('hidden');
    });
    
    
    $('.widgets-info .info-block p').on('click', function(e){
        e.preventDefault();
        $('.intro-all.intro-call').toggleClass('active');
        $('body').toggleClass('hidden');
    });
    $('.pop-up-call .btn-menu-pop-up').on('click', function(){
        if($('.mobile-header-open').hasClass('phones-active')){
            $('.intro-all.intro-call').toggleClass('active');
            $('body').toggleClass('hidden');
            $('.mobile-header-open').toggleClass('phones-active');
            $('.intro-mobile-header-open').slideToggle(300);
        }
        else if($('.mobile-header .btn-menu').hasClass('active')){
            $('.intro-all.intro-call').toggleClass('active');
            $('body').toggleClass('hidden');
            $('.mobile-header .btn-menu').toggleClass('active');
            $('.intro-mobile-header-open').slideToggle(300);
        }
        else{
            $('.intro-all.intro-call').toggleClass('active');
            $('body').toggleClass('hidden');
        }
    });
    
    $('.btn-call').on('click', function(e){
        e.preventDefault();
        $('.intro-all.intro-call').toggleClass('active');
        $('body').toggleClass('hidden');
    });
    
    $('.checout-content .cart-finish .order').on('click', function(e){
        e.preventDefault();
        $('.checout-content').toggleClass('active');
        $('.checout-content-finish').toggleClass('active');
    });
    
    var $body;
    $body = $('body');
    $body.find('.user-phone').each(function(){
          $(this).mask("+ 9 9   9 9 9   9 9 9   9 9   9 9", {autoсlear: false});
      });
    $body.on('keyup','.user-phone',function(){
      var phone = $(this),
          phoneVal = phone.val(),
          form = $(this).parents('form');
      if ( (phoneVal.indexOf("_") != -1) || phoneVal == '' ) {
        form.find('.btn_submit').attr('disabled',true);
      } else {
        form.find('.btn_submit').removeAttr('disabled');
      }
    });
    
    var $oneClick;
    $oneClick = $('body');
    $oneClick.find('#user_phone-one-click').each(function(){
          $(this).mask("+38 999 999 99 99", {autoсlear: false});
      });
    $oneClick.on('keyup','#user_phone-one-click',function(){
      var phone = $(this),
          phoneVal = phone.val(),
          form = $(this).parents('form');
      if ( (phoneVal.indexOf("_") != -1) || phoneVal == '' ) {
        form.find('.one-click-btn').attr('disabled',true);
      } else {
        form.find('.one-click-btn').removeAttr('disabled');
      }
    });
    
    
    $('.go_to').click( function(){ // ловим клик по ссылке с классом go_to
	var scroll_el = $(this).attr('href'); // возьмем содержимое атрибута href, должен быть селектором, т.е. например начинаться с # или .
        if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
	    $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 3000); // анимируем скроолинг к элементу scroll_el
        }
	    return false; // выключаем стандартное действие
    });
    
    
    
    
    
    


    if ($(window).width() < 768) {
        $('.basket').on('click', function(e){
            e.preventDefault();
            $('html, body').animate({scrollTop: 0},500);
            $('.intro-cart-mobile').toggleClass('active');
            $('body').toggleClass('hidden');
        });
        $('.intro-cart-mobile .pop-up-cart .btn-menu-pop-up').on('click', function(){
            $('.intro-cart-mobile').toggleClass('active');
            $('body').toggleClass('hidden');
        }); 
    }
    else{
        $('.basket').on('click', function(e){
            e.preventDefault();
            $('.intro-cart').toggleClass('active');
            $('body').toggleClass('hidden');
        });
        $('.intro-cart .pop-up-cart .btn-menu-pop-up').on('click', function(){
            $('.intro-cart').toggleClass('active');
            $('body').toggleClass('hidden');
        });
    }
    
    $('.minus').on('click', function(){
        var valInput = parseInt($(this).parent().children('.inputCount').val());
        if(valInput <= 0){   
        }
        else{
            valInput -= 1;
            $(this).parent().children('.inputCount').val(valInput);
        }
    });
    $('.plus').on('click', function(){
        var valInput = parseInt($(this).parent().children('.inputCount').val());
        valInput += 1;
        $(this).parent().children('.inputCount').val(valInput);
    });
    
    /*$('.filter-one').on('click', function(){
        $(this).find('.filter-content').slideToggle();
    });*/
    
   /* $('.product.varient').on('mouseover', function(){
        $(this).find('.price-varient').slideToggle();
    });
    $('.product.varient').on('mouseout', function(){
        $(this).find('.price-varient').slideToggle(300);
    });*/
    if ($(window).width() < 768) {
        $('.recommenda-product .product-block').addClass('slider-product-mob');
        $('.slider-product-mob').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            responsive: [
                {
                  breakpoint: 560,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                  }
                }
            ]
        }); 
        $('.watched-product .product-block').addClass('slider-product-watched-mob');
        $('.slider-product-watched-mob').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            responsive: [
                {
                  breakpoint: 560,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                  }
                }
            ]
        });
    }
    else{
        $('.recommenda-product .product-block').removeClass('slider-product-mob');
        $('.watched-product .product-block').removeClass('slider-product-watched-mob');
    }    
});